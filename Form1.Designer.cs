﻿namespace PlayNote {
	partial class Form1 {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.functionBox = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.waveImage = new System.Windows.Forms.PictureBox();
			this.panel3 = new System.Windows.Forms.Panel();
			this.builtInBox = new System.Windows.Forms.ComboBox();
			this.button1 = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.playButton = new System.Windows.Forms.Button();
			this.panel4 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.songBox = new System.Windows.Forms.TextBox();
			this.keyChartButton = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.waveImage)).BeginInit();
			this.panel3.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel4.SuspendLayout();
			this.SuspendLayout();
			// 
			// functionBox
			// 
			this.functionBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.functionBox.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.functionBox.Location = new System.Drawing.Point(0, 169);
			this.functionBox.Multiline = true;
			this.functionBox.Name = "functionBox";
			this.functionBox.Size = new System.Drawing.Size(655, 372);
			this.functionBox.TabIndex = 6;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(3, 9);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(51, 13);
			this.label3.TabIndex = 5;
			this.label3.Text = "Function:";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.waveImage);
			this.panel1.Controls.Add(this.panel3);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(655, 139);
			this.panel1.TabIndex = 9;
			// 
			// waveImage
			// 
			this.waveImage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.waveImage.Location = new System.Drawing.Point(0, 35);
			this.waveImage.Name = "waveImage";
			this.waveImage.Size = new System.Drawing.Size(655, 104);
			this.waveImage.TabIndex = 8;
			this.waveImage.TabStop = false;
			this.waveImage.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.label3);
			this.panel3.Controls.Add(this.builtInBox);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(655, 35);
			this.panel3.TabIndex = 9;
			// 
			// builtInBox
			// 
			this.builtInBox.FormattingEnabled = true;
			this.builtInBox.Location = new System.Drawing.Point(60, 6);
			this.builtInBox.Name = "builtInBox";
			this.builtInBox.Size = new System.Drawing.Size(121, 21);
			this.builtInBox.TabIndex = 7;
			this.builtInBox.SelectedValueChanged += new System.EventHandler(this.builtInBox_SelectedValueChanged);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(131, 3);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 6;
			this.button1.Text = "Generate";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.textBox1);
			this.panel2.Controls.Add(this.keyChartButton);
			this.panel2.Controls.Add(this.playButton);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel2.Location = new System.Drawing.Point(0, 541);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(655, 25);
			this.panel2.TabIndex = 10;
			// 
			// textBox1
			// 
			this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBox1.Location = new System.Drawing.Point(0, 0);
			this.textBox1.Name = "textBox1";
			this.textBox1.ReadOnly = true;
			this.textBox1.Size = new System.Drawing.Size(505, 20);
			this.textBox1.TabIndex = 5;
			this.textBox1.Text = "Place cursor here to play like a piano.";
			this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.piano_KeyDown);
			this.textBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.piano_KeyUp);
			this.textBox1.Leave += new System.EventHandler(this.piano_Leave);
			// 
			// playButton
			// 
			this.playButton.Dock = System.Windows.Forms.DockStyle.Right;
			this.playButton.Location = new System.Drawing.Point(580, 0);
			this.playButton.Name = "playButton";
			this.playButton.Size = new System.Drawing.Size(75, 25);
			this.playButton.TabIndex = 6;
			this.playButton.Text = "Play";
			this.playButton.UseVisualStyleBackColor = true;
			this.playButton.Visible = false;
			this.playButton.Click += new System.EventHandler(this.playButton_Click);
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.label1);
			this.panel4.Controls.Add(this.button1);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel4.Location = new System.Drawing.Point(0, 139);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(655, 30);
			this.panel4.TabIndex = 11;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(3, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(122, 13);
			this.label1.TabIndex = 7;
			this.label1.Text = "theta, step, maxStep) =>";
			// 
			// songBox
			// 
			this.songBox.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.songBox.Location = new System.Drawing.Point(0, 442);
			this.songBox.Multiline = true;
			this.songBox.Name = "songBox";
			this.songBox.Size = new System.Drawing.Size(655, 99);
			this.songBox.TabIndex = 12;
			this.songBox.Visible = false;
			// 
			// keyChartButton
			// 
			this.keyChartButton.Dock = System.Windows.Forms.DockStyle.Right;
			this.keyChartButton.Location = new System.Drawing.Point(505, 0);
			this.keyChartButton.Name = "keyChartButton";
			this.keyChartButton.Size = new System.Drawing.Size(75, 25);
			this.keyChartButton.TabIndex = 7;
			this.keyChartButton.Text = "Key Chart";
			this.keyChartButton.UseVisualStyleBackColor = true;
			this.keyChartButton.Click += new System.EventHandler(this.keyChartButton_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(655, 566);
			this.Controls.Add(this.songBox);
			this.Controls.Add(this.functionBox);
			this.Controls.Add(this.panel4);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Name = "Form1";
			this.Text = "Note Player";
			this.Leave += new System.EventHandler(this.piano_Leave);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.waveImage)).EndInit();
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panel4.ResumeLayout(false);
			this.panel4.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox functionBox;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.ComboBox builtInBox;
		private System.Windows.Forms.PictureBox waveImage;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox songBox;
		private System.Windows.Forms.Button playButton;
		private System.Windows.Forms.Button keyChartButton;
	}
}

