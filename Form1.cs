﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.CSharp;
using NAudio.Wave;

namespace PlayNote {
	public partial class Form1 : Form {
		const int SAMPLE_COUNT = 6615;
		private const int SAMPLE_RATE = 44100;
		enum WaveFunction { Sine, Square, Sawtooth, Triange };

		double halfStepConst = 1.0594630943592952645618252949463417007792043174941856;
		string keys = "Q2W3ER5T6Y7UI|ZSXDCVGBHNJM¼";

		private Dictionary<char, WaveOut> notes = new Dictionary<char, WaveOut>();

		private Func<double, int, int, double> wave;
		string Function { get { return functionBox.Text; } set { functionBox.Text = value; } }
		string Song { get { return songBox.Text; } set { songBox.Text = value; } }

		KeyChart keyChart;

		public Form1() {
			InitializeComponent();
			foreach (WaveFunction f in Enum.GetValues(typeof(WaveFunction))) {
				builtInBox.Items.Add(f);
			}
			builtInBox.SelectedIndex = 0;
		}

		private void createNotes() {
			Cursor = Cursors.WaitCursor;
			string errors = getMethod(Function, out wave);
			if (errors != null) {
				MessageBox.Show(errors);
				return;
			}

			foreach (var note in notes.Values) {
				if (note != null) {
					note.Dispose();
				}
			}

			double freq = 261.62556530059868; // C
			for (int c = 0; c < keys.Length; c++) {
				char key = keys[c];
				if (key == '|') {
					freq /= halfStepConst;
					continue;
				}
				notes[key] = CreateNote((int)freq, wave);
				freq *= halfStepConst;
			}

			waveImage.Refresh();
			Cursor = Cursors.Arrow;
		}

		public WaveOut CreateNote(int frequency, Func<double, int, int, double> waveFunction) {
			var wave = new FuncWaveProvider32();
			wave.SetWaveFormat(SAMPLE_RATE, 1); // 16kHz mono
			wave.Frequency = frequency;
			wave.Amplitude = 0.25f;
			wave.WaveFunction = waveFunction;
			WaveOut waveOut = new WaveOut();
			waveOut.Init(wave);

			return waveOut;
		}

		private static string getMethod(string lambdaExpression, out Func<double, int, int, double> wave) {
			string source = @"
			using System;
			namespace LambdaNamespace {
				public class LambdaClass {
					public const double E = 2.71828;
					public const double PI = 3.14159;
					public const double e = 2.71828;
					public const double pi = 3.14159;
					public static double sin(double x) { return Math.Sin(x); }
					public static double Sin(double x) { return Math.Sin(x); }
					public static double asin(double x) { return Math.Asin(x); }
					public static double Asin(double x) { return Math.Asin(x); }
					public static double cos(double x) { return Math.Cos(x); }
					public static double Cos(double x) { return Math.Cos(x); }
					public static double acos(double x) { return Math.Acos(x); }
					public static double Acos(double x) { return Math.Acos(x); }
					public static double tan(double x) { return Math.Tan(x); }
					public static double Tan(double x) { return Math.Tan(x); }
					public static double atan(double x) { return Math.Atan(x); }
					public static double Atan(double x) { return Math.Atan(x); }
					public static double atan2(double x, double y) { return Math.Atan2(x, y); }
					public static double Atan2(double x, double y) { return Math.Atan2(x, y); }
					public static double abs(double x) { return Math.Abs(x); }
					public static double Abs(double x) { return Math.Abs(x); }
					public static double ceiling(double x) { return Math.Ceiling(x); }
					public static double Ceiling(double x) { return Math.Ceiling(x); }
					public static double floor(double x) { return Math.Floor(x); }
					public static double Floor(double x) { return Math.Floor(x); }
					public static double round(double x) { return Math.Round(x); }
					public static double Round(double x) { return Math.Round(x); }
					public static double log(double x) { return Math.Log(x); }
					public static double Log(double x) { return Math.Log(x); }
					public static double log(double x, double newBase) { return Math.Log(x, newBase); }
					public static double Log(double x, double newBase) { return Math.Log(x, newBase); }
					public static double log10(double x) { return Math.Log10(x); }
					public static double Log10(double x) { return Math.Log10(x); }
					public static double exp(double x) { return Math.Exp(x); }
					public static double Exp(double x) { return Math.Exp(x); }
					public static double sec(double x) { return 1.0 / Math.Cos(x); }
					public static double Sec(double x) { return 1.0 / Math.Cos(x); }
					public static double csc(double x) { return 1.0 / Math.Sin(x); }
					public static double Csc(double x) { return 1.0 / Math.Sin(x); }
					public static double ctan(double x) { return 1.0 / Math.Tan(x); }
					public static double Ctan(double x) { return 1.0 / Math.Tan(x); }
					public Func<double, int, int, double> LambdaMethod() { 
						return (theta, step, maxStep) => " + lambdaExpression + @";
					}
				}
			}";

			CompilerParameters parameters = new CompilerParameters();
			parameters.GenerateExecutable = false;
			CSharpCodeProvider codeProvider = new CSharpCodeProvider();
			CompilerResults results = codeProvider.CompileAssemblyFromSource(parameters, source);

			if (!results.Errors.HasErrors && !results.Errors.HasWarnings) {
				Assembly ass = results.CompiledAssembly;

				object lambdaClassInstance = ass.CreateInstance("LambdaNamespace.LambdaClass");
				MethodInfo waveMethod = lambdaClassInstance.GetType().GetMethod("LambdaMethod");

				wave = (Func<double, int, int, double>)waveMethod.Invoke(lambdaClassInstance, new object[] { });
				return null;
			} else {
				wave = null;
				StringBuilder sb = new StringBuilder();
				foreach (var error in results.Errors) {
					sb.AppendLine(error.ToString());
				}
				return sb.ToString();
			}
		}

		private void piano_KeyDown(object sender, KeyEventArgs e) {
			char key = ((char)e.KeyCode);
			if (!notes.ContainsKey(key)) { return; }

			notes[key].Play();
		}

		private void piano_KeyUp(object sender, KeyEventArgs e) {
			char key = ((char)e.KeyCode);
			if (!notes.ContainsKey(key)) { return; }

			notes[key].Stop();
		}

		private void button1_Click(object sender, EventArgs e) {
			builtInBox.Text = "(Custom)";
			createNotes();
		}

		private void piano_Leave(object sender, EventArgs e) {
			foreach (var note in notes.Values) {
				note.Stop();
			}
		}

		private void builtInBox_SelectedValueChanged(object sender, EventArgs e) {
			switch ((WaveFunction)builtInBox.SelectedItem) {
				case WaveFunction.Sine:
					Function = "Sin(step * theta)";
					createNotes();
					break;
				case WaveFunction.Sawtooth:
					Function = "Atan(Ctan(step*theta/2))";
					createNotes();
					break;
				case WaveFunction.Triange:
					Function = "Abs(Atan(Ctan(step*theta/2)))";
					createNotes();
					break;
				case WaveFunction.Square:
					Function = "Abs(Sin(step*theta))*Csc(step*theta)";
					createNotes();
					break;
			}
		}

		private void pictureBox1_Paint(object sender, PaintEventArgs e) {
			if (wave == null) { return; }
			e.Graphics.FillRectangle(Brushes.Black, e.ClipRectangle);
			float theta = (float)(440 * 2 * Math.PI / (float)SAMPLE_RATE);
			int sample = 0;
			int x = 1;
			float prev = (float)(wave(theta, 0, SAMPLE_COUNT)) * waveImage.Height / 4 + waveImage.Height / 2;
			if (float.IsNaN(prev) || float.IsInfinity(prev)) { prev = waveImage.Height / 2; }
			using (Pen pen = new Pen(Color.FromArgb(0, 255, 0), 2)) {
				for (int n = waveImage.Width; n < SAMPLE_COUNT; n++) {
					try {
						float currentVal = FuncWaveProvider32.GetValue(theta, n, SAMPLE_COUNT, wave) * waveImage.Height / 4 + waveImage.Height / 2;
						e.Graphics.DrawLine(pen, x - 1, prev, x, currentVal);
						prev = currentVal;
					} catch { }
					sample++;
					x++;
					if (sample >= SAMPLE_RATE) sample = 0;
				}
			}
		}

		private void playButton_Click(object sender, EventArgs e) {
			Cursor = Cursors.WaitCursor;
			playSong();
			Cursor = Cursors.Arrow;
		}

		private void playSong() {
			foreach (string group in Song.Split(' ')) {
				char note = group[0];
				if (note == ',') { note = '¼'; }
				if (!notes.ContainsKey(note)) {
					MessageBox.Show(string.Format("Unrecognized note: {0}.", note));
					return;
				}
				
				string durationString = group.Substring(1);
				int duration = 0;
				if (!int.TryParse(durationString, out duration)) {
					MessageBox.Show(string.Format("Unable to parse duration of: {0}.", durationString));
					return;
				}

				notes[note].Play();
				Thread.Sleep(duration);
				notes[note].Stop();
			}
		}

		private void keyChartButton_Click(object sender, EventArgs e) {
			if (keyChart == null) {
				keyChart = new KeyChart();
				keyChart.KeyDown += piano_KeyDown;
				keyChart.KeyUp += piano_KeyUp;
				keyChart.Leave += piano_Leave;
				keyChart.Disposed += keyChart_Disposed;
				keyChart.FormClosing += keyChart_FormClosing;
				keyChart.Show();
			} else {
				keyChart.Show();
				keyChart.BringToFront();
			}
		}

		void keyChart_FormClosing(object sender, FormClosingEventArgs e) {
			keyChart.Hide();
			e.Cancel = true;
		}

		void keyChart_Disposed(object sender, EventArgs e) {
			keyChart.KeyDown -= piano_KeyDown;
			keyChart.KeyUp -= piano_KeyUp;
			keyChart.Leave -= piano_Leave;
			keyChart.Disposed -= keyChart_Disposed;
			keyChart = null;
		}
	}
}