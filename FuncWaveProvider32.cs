using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.CSharp;
using NAudio.Wave;

namespace PlayNote {
	public class FuncWaveProvider32 : WaveProvider32 {
		public Func<double, int, int, double> WaveFunction { get; set; }

		int sample;

		public FuncWaveProvider32() {
			Frequency = 1000;
			Amplitude = 0.25f; // let's not hurt our ears           
		}

		public float Frequency { get; set; }
		public float Amplitude { get; set; }

		public override int Read(float[] buffer, int offset, int sampleCount) {
			int sampleRate = WaveFormat.SampleRate;
			float theta = (float)(Frequency * 2 * Math.PI / (float)sampleRate);
			for (int n = 0; n < sampleCount; n++) {
				try {
					float value = Amplitude*GetValue(theta, sample, sampleCount, WaveFunction);
					buffer[n + offset] = value;
				} catch { }
				//buffer[n + offset] = (float)(Amplitude * Math.Sin((2 * Math.PI * sample * Frequency) / sampleRate));
				sample++;
				if (sample >= sampleRate) sample = 0;
			}
			return sampleCount;
		}

		public static float GetValue(float theta, int sample, int sampleCount, Func<double, int, int, double> waveFunction) {
			float value = (float)(waveFunction(theta, sample, sampleCount));
			if (float.IsNaN(value)) { value = 0; } 
			else if (float.IsPositiveInfinity(value)) { value = float.MaxValue; } 
			else if (float.IsPositiveInfinity(value)) { value = float.MinValue; }
			return value;
		}
	}
}

