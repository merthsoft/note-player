# README #

This program lets you generate waves and hear what they sound like when played.

### How do I get set up? ###

Get the code and run it, or download from [here](http://merthsoft.com/PlayNote.zip). You will see something like this:

![Note_Player_2014-07-09_19-08-19.png](https://bitbucket.org/repo/9AbERe/images/2093135813-Note_Player_2014-07-09_19-08-19.png)

Place your cursor in the text box at the bottom, and use the keyboard to play like a piano:

![KeyChart.png](https://bitbucket.org/repo/9AbERe/images/2839058080-KeyChart.png)

Use the drop down at the top to choose different built-in wave types, or type in the box and click "Generate" to create your own:

![Note_Player_2014-07-09_19-08-42.png](https://bitbucket.org/repo/9AbERe/images/3347279873-Note_Player_2014-07-09_19-08-42.png)

Most math functions I've aliased to just their names, such as *Sin* or *sin* instead of *Math.Sin*.

This program uses [NAudio](http://naudio.codeplex.com/) for the sound generation.